#!/bin/bash

set -x

cad=`awk 'BEGIN { ORS = " " } { print }' subj.txt`

date

for elem in `cat subj.txt` ; do

  docker run \
     -v /home/dnanexus/data:/data  \
     -v /home/dnanexus/rap_fsl_script:/scripts  \
     bip_image_no_fs \
     /bin/bash  /scripts/MMORF/script.sh $elem
  if [ -d $elem/MMORF ] ; then
     ~/rap_fsl_script/upload_subjects.sh $elem
  fi
done

date


