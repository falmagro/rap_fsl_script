#!/bin/bash

set -x
set -e

DNADirectory="FBP_TESTS_2"

if [ "$2" == "" ] ; then
  num_cores="16"
else
  num_cores="$2"
fi


#TODO: Allow user to pick a non-GPU machine
machine="mem2_ssd1_gpu_x16";

if [ "$1" == "" ] ; then
  subj_file="subj.txt"
else
  subj_file="$1"
fi

if [ ! -f $subj_file ] ; then
   Echo "ERROR: $subj_file does not exist"
   exit 1;
fi

# Upload a file with the subjects to process
nam=`date +"%m-%d-%y_%H-%M-%S.%s"`
tmp=`tmpnam`
cp $subj_file $tmp

# Upload the file with the IDs to process
dx upload --path $DNADirectory:subjects/s_$nam.txt $tmp

# Create the specified machine
jID=$(dx run cloud_workstation -y --allow-ssh --instance-type $machine \
   --priority high --input max_session_length=24h | grep "Job ID" | awk '{print $3}')
echo "@SIGNAL@ $jID"

# Send (via ssh) the specified commands to the machine
cmd_1="dx download $DNADirectory:subjects/s_$nam.txt"  # Download the list of subjects to process
cmd_2="mv s_$nam.txt subj.txt"                         # Rename the list of subjects to a common name
cmd_3="git clone https://git.fmrib.ox.ac.uk/falmagro/rap_fsl_script.git"   # Download repository
cmd_4="chmod 755 ~/rap_fsl_script/*.sh ~/rap_fsl_script/killme ~/subj.txt" # Make everything runnable
cmd_5="nohup ~/rap_fsl_script/proc_all.sh ${num_cores} &> log_${nam}.txt" # Run the processing

dx ssh $jID -f "nohup /bin/bash -l -c '$cmd_1 ; $cmd_2 ; $cmd_3 ; $cmd_4 ; $cmd_5 ;'" &
