#!/bin/bash

#set -x

export script_dir="/scripts/MMORF/"
export FSLDIR=/usr/local/fsl/
source ${FSLDIR}/etc/fslconf/fsl.sh
export FSLOUTPUTTYPE="NIFTI_GZ"
export FSLSUB_CONF="$BIPDIR/data/fsl_sub.yml"
alias ll="ls -larth"

SUBJECT="$1"

ID=${SUBJECT:1:10}
VISIT=${SUBJECT:0:1}
START=${SUBJECT:1:2}
export DNADirectory=FBP_TESTS_2

cd /data/

export sdir="/data/${SUBJECT}/"
export odir="${sdir}/MMORF/"
mkdir -p $odir

date;

${FSLDIR}/bin/fslmaths \
   ${sdir}/T2_FLAIR/T2_FLAIR.nii.gz \
   -div ${sdir}/T1/T1_fast/T1_brain_bias.nii.gz \
   ${sdir}/T2_FLAIR/T2_FLAIR_unbiased.nii.gz

${FSLDIR}/bin/fslmaths \
   ${sdir}/T1/T1.nii.gz \
   -div ${sdir}/T1/T1_fast/T1_brain_bias.nii.gz \
   ${sdir}/T1/T1_unbiased.nii.gz

${FSLDIR}/bin/flirt \
   -in ${sdir}/T1/T1_unbiased_brain \
   -ref ${FSLDIR}/data/omm/Oxford-MM-1/OMM-1_T1_brain \
   -omat ${odir}/T1_to_OMM_flirt.mat

${FSLDIR}/bin/convert_xfm \
   -omat ${odir}/DTI_to_T1_to_OMM_flirt.mat \
   -concat ${odir}/T1_to_OMM_flirt.mat \
   ${sdir}/fieldmap/fieldmap_iout_to_T1.mat

ln -s ${sdir}/fieldmap/fieldmap_mask_ud.nii.gz \
   ${sdir}/dMRI/dMRI/nodif_brain_mask_ud.nii.gz

chmod 755 ${script_dir}/bb_select_dwi_vols

${script_dir}/bb_select_dwi_vols \
       ${sdir}/dMRI/dMRI/data_ud.nii.gz \
       ${sdir}/dMRI/dMRI/bvals \
       ${sdir}/dMRI/dMRI/data_ud_1_shell \
       1000 1 ${sdir}/dMRI/dMRI/bvecs

${FSLDIR}/bin/dtifit \
   -k ${sdir}/dMRI/dMRI/data_ud_1_shell \
   -m ${sdir}/dMRI/dMRI/nodif_brain_mask_ud \
   -r ${sdir}/dMRI/dMRI/data_ud_1_shell.bvec \
   -b ${sdir}/dMRI/dMRI/data_ud_1_shell.bval \
   -o ${sdir}/dMRI/dMRI/new_dti \
   --save_tensor &> /dev/null

date; 

${FSLDIR}/bin/mmorf \
   --version \
   --config ${script_dir}/config_T1_FLAIR_DTI.ini \
   --img_warp_space ${FSLDIR}/data/omm/Oxford-MM-1/OMM-1_T1_head \
   --img_ref_scalar ${FSLDIR}/data/omm/Oxford-MM-1/OMM-1_T1_head \
   --img_mov_scalar ${sdir}/T1/T1_unbiased \
   --aff_ref_scalar ${FSLDIR}/etc/flirtsch/ident.mat \
   --aff_mov_scalar ${odir}/T1_to_OMM_flirt.mat \
   --mask_ref_scalar ${FSLDIR}/data/omm/Oxford-MM-1/OMM-1_T1_brain_mask_weighted \
   --img_ref_scalar ${FSLDIR}/data/omm/Oxford-MM-1/OMM-1_T2_FLAIR_head \
   --img_mov_scalar ${sdir}/T2_FLAIR/T2_FLAIR_unbiased \
   --aff_ref_scalar ${FSLDIR}/etc/flirtsch/ident.mat \
   --aff_mov_scalar ${odir}/T1_to_OMM_flirt.mat \
   --img_ref_tensor ${FSLDIR}/data/omm/Oxford-MM-1/OMM-1_DTI_tensor \
   --img_mov_tensor ${sdir}/dMRI/dMRI/new_dti_tensor \
   --aff_ref_tensor ${FSLDIR}/etc/flirtsch/ident.mat \
   --aff_mov_tensor ${odir}/DTI_to_T1_to_OMM_flirt.mat \
   --mask_ref_tensor ${script_dir}/OMM-1_DTI_reg_mask_soft \
   --mask_mov_tensor ${sdir}/dMRI/dMRI/nodif_brain_mask_ud \
   --warp_out ${odir}/T1_FLAIR_DTI_MMORF_warp \
   --jac_det_out ${odir}/T1_FLAIR_DTI_MMORF_jac \
   --bias_out ${odir}/T1_FLAIR_DTI_MMORF_bias > ${odir}/log_T1_FLAIR_DTI.txt ; 

date

# MMORF command without DTI
date; 
${FSLDIR}/bin/mmorf \
   --version \
   --config ${script_dir}/config_T1_FLAIR.ini \
   --img_warp_space ${FSLDIR}/data/omm/Oxford-MM-1/OMM-1_T1_head \
   --img_ref_scalar ${FSLDIR}/data/omm/Oxford-MM-1/OMM-1_T1_head \
   --img_mov_scalar ${sdir}/T1/T1_unbiased \
   --aff_ref_scalar ${FSLDIR}/etc/flirtsch/ident.mat \
   --aff_mov_scalar ${odir}/T1_to_OMM_flirt.mat \
   --mask_ref_scalar ${FSLDIR}/data/omm/Oxford-MM-1/OMM-1_T1_brain_mask_weighted \
   --img_ref_scalar ${FSLDIR}/data/omm/Oxford-MM-1/OMM-1_T2_FLAIR_head \
   --img_mov_scalar ${sdir}/T2_FLAIR/T2_FLAIR_unbiased \
   --aff_ref_scalar ${FSLDIR}/etc/flirtsch/ident.mat \
   --aff_mov_scalar ${odir}/T1_to_OMM_flirt.mat \
   --warp_out ${odir}/T1_FLAIR_MMORF_warp \
   --jac_det_out ${odir}/T1_FLAIR_MMORF_jac \
   --bias_out ${odir}/T1_FLAIR_DTI_MMORF_bias > ${odir}/log_T1_FLAIR.txt ;

date
