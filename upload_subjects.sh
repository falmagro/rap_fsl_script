#!/bin/bash

set -x

elem="$1"

zip -r "$elem.zip" $elem/MMORF
dx upload $elem.zip --path FBP_TESTS_2:MMORF_outputs/
