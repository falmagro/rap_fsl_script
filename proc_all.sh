#!/bin/bash
# Note: Change FBP_TESTS_2 to the appropriate DNANexus directory

set -x

DNADirectory="FBP_TESTS_2"
num_cores="$1"
nam=`date +"%m-%d-%y_%H-%M-%S.%s"`

date;
sudo apt-get install -y git-lfs dcm2niix moreutils parallel

date
cd $HOME;
mkdir docker;
cd docker ;
if [ ! -f ./bip_image_no_fs.tar ] ; then
  dx download FBP_TESTS_2:/docker/bip_image_no_fs.tar
  docker load --input ./bip_image_no_fs.tar ;
fi

cd ..

date
mkdir -p data ;
mv subj.txt data/
cd data


# Split all subjects by the number of cores
num_lines=$(cat subj.txt | wc -l)
num=$(echo "$num_lines / $num_cores" | bc)

if [[ ! "${num_lines}" -lt "${num_cores}" ]] ; then
  split -d -l ${num} -a 4 subj.txt  subj_
else
  cp subj.txt subj_0000.txt
fi

# Run the downloading jobs in parallel
for subj_file in subj_* ; do
   echo "~/rap_fsl_script/download_subjects.sh $subj_file"
done > job_subjects.txt
cat job_subjects.txt | parallel --jobs ${num_cores}

# Process the data
date
~/rap_fsl_script/proc_subjects.sh subj.txt ${num_cores} ;

# Upload outputs to permanent storage in parallel
#date
#for subj_file in subj_* ; do
#  echo "~/rap_fsl_script/upload_subjects.sh $subj_file;"
#done > job_subjects.txt
#cat job_subjects.txt | parallel --jobs ${num_cores}

rm subj_????

t=$(ls ~/log_*.txt | tail -n 1)

if [ -f "$t" ] ; then
  dx upload $t --path FBP_TESTS_2:logs/
fi

# Kill the machine
. ~/rap_fsl_script/killme
