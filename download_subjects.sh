#!/bin/bash

fileExists() {
  # The next command will have exit status 0 (no error) if the file exists
  dx ls "$1" &> /dev/null;

  # Check the exit status of last command
  if [ "$?" == "0" ] ; then
    return 0;
  else
    return 1;
  fi
}

downloadAndMove(){
  modality=`echo "$1" | sed 's|_| |g'`;
  number="$2"
  file="$DNADirectory:/Bulk/Brain MRI/$modality/$START/${ID}_202${number}_${VISIT}_0.zip"

  if fileExists "$file" ; then
    mkdir -p $HOME/data/$ID/
    pushd $HOME/data/$ID/
    dx download "$file"
    if [ -f ${ID}_202${number}_${VISIT}_0.zip ] ; then
         unzip ${ID}_202${number}_${VISIT}_0.zip;
         rm ${ID}_202${number}_${VISIT}_0.zip
    fi
    popd
  fi
}

set -x

cad=""
new_nam=`echo $1 | sed 's|.txt||g'`

for SUBJECT in `cat $1` ; do

   export ID=${SUBJECT:1:10}
   export VISIT=${SUBJECT:0:1}
   export START=${SUBJECT:1:2}
   export DNADirectory=FBP_TESTS_2

   date;
   echo $SUBJECT

   downloadAndMove "T1"       "52";
   downloadAndMove "T2_FLAIR" "53";
   downloadAndMove "dMRI"     "50";
   mv ~/data/$ID ~/data/$SUBJECT
done

